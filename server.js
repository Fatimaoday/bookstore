const express = require('express');
const app = express();
const mongoose = require('mongoose')
const booksRoutes = require('./routes/book');
const usersRoutes = require('./routes/user');
const fileUpload = require('express-fileupload');


mongoose.connect('mongodb://fatima1:fatima1@ds141654.mlab.com:41654/bookstore')
mongoose.connection.on('connected', ()=> {
  console.log('\x1b[34m',"Database connected ....")
})

//MiddleWares
app.use(express.json());
app.use('/api/book',fileUpload(), booksRoutes);
app.use('/api/user', usersRoutes);



const port = 5000;
app.listen(port, () => console.log(`Server running on port ${port}`));