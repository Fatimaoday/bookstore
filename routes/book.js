const express = require('express');
const router = express.Router();
const Joi = require('joi');
const mongoose = require('mongoose')
const Book = require('../models/book')
const uuidv1 = require('uuid/v1');
const auth = require('../middleware/auth.js')



// Getting all books
router.get('/', (req, res) => {

  Book.find().then(result => {
    res.send(result)
  }).catch(err => {
    res.status(404).send(err)
  })

});

// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVjMjJiYWVjNjM0ZWVmMWYxODFhOGJmMCIsImlhdCI6MTU0NTc3OTk0OX0.XirbxyqfnXhGg3DjT4SG9yrlYkM2oQ63Gcu0y5GML5M

//posting a new book
router.post('/', auth,(req, res) => {
    
  let image = req.files.img;
  imgName = uuidv1();

  let pdf = req.files.pdf;
  pdfName = uuidv1();


  image.mv(`./public/${imgName}.png`, function(err) {
    pdf.mv(`./public/${pdfName}.pdf`, function(err) {

        const book =  new Book ({
            _id: new mongoose.Types.ObjectId(),
            title:  req.body.title,
            author:  req.body.author,
            year:  req.body.year,
            image: `/public/${imgName}.png` ,
            pdf:  `./public/${pdfName}.png`,
            
          })
          book.save()
          res.send('Book Saved');

    });
  });
  
});


//deleting a book
router.delete('/:id', (req, res) => {
  Book.remove({_id: req.params.id}).then(result => {
    res.send('item deleted :(')
    console.log('item deleted :(')
  }).catch(err => {
    res.status(404).send(err);
  });
});



module.exports = router;
