const express = require('express');
const router = express.Router();
const Joi = require('joi');
const mongoose = require('mongoose')
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../models/user')


// Getting information of a user
router.get('/:id', (req, res) => {
  let user = User.findById(req.params.id).then(result => {
    if(!result){
      res.status(404).send('There is no such user');
    }
    res.send(result);
  }).catch(err => {
    res.status(404).send(err);
  })

});

//  Regastiration a new user
router.post('/register', (req, res) => {

  bcrypt.genSalt(10).then(salt => {
    bcrypt.hash(req.body.password, salt).then(hashed => {
      const user = new User({
        _id: new mongoose.Types.ObjectId(),
        username: req.body.username,
        password: hashed,
        email: req.body.email,
        isAdmin: false
      });
      user.save().then(result => {
        const token = jwt.sign({id: result._id}, 'key')
        res.header({'X-auth-token': token}).send("a new user has registered");
        console.log("a new user has registered")
      }).catch(err => {
        console.log(err)
      })
    })
  });
});

//user login
router.post('/login', (req, res) => {

  if(req.body.username && req.body.password){

    User.find({ username : req.body.username})
    .then(result => {
      bcrypt.compare(req.body.password, result[0].password, function(err, ress) {
        if(ress){

          const token = jwt.sign({id: result._id}, 'someKey')
          res.header({'X-auth-token': token}).send("now you're loged in");

        }
        else{
          res.send('your information is wrong')
        }
      });  
    }).catch(err => {
      console.log(err)
      res.status(404).send(err)
    })
  }

});


function userValidating(user) {
  const userSchema = {
    'name': Joi.string().required().min(3),
    'email': Joi.string().required(),
    'password':Joi.string().required()
  }
  return Joi.validate(user, userSchema);
}



module.exports = router;
