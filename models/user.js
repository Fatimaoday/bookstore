const mongoose = require('mongoose');


const userSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  username: {
    type: String,
    required: [true, 'Username Is Required']
  },
  email: {
    type: String,
    required: [true, 'email Is Required']
  },
  password: {
    type: String,
    required: [true, 'password Is Required']
  },
  isAdmin: Boolean
});

module.exports = mongoose.model('User', userSchema);
