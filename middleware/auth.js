const jwt = require('jsonwebtoken');
const mongoose = require('mongoose')
const User = require('../models/user')


function auth(req, res, next) {

  const token = req.headers.token;
    if(token){
      try {
        let payload = jwt.verify(token, 'key');

        let user = User.findById(payload.id).then(result => {
              if(result.isAdmin == true){ next() }
          }).catch(err => {
            res.status(404).send('you are not the admin');
          })
      } catch (err) {
        res.status(400).send(err);
      }
    }else{
      res.send('you need to login');
    }
  }
  
  
  module.exports = auth;